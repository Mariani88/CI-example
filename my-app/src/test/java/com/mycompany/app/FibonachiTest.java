package com.mycompany.app;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FibonachiTest {

	private Fibonachi fibonachi;

	@Before
	public void setUp() throws Exception {
		fibonachi = new Fibonachi();
	}

	@Test
	public void whenNEqualsZeroThenFibonachiIsZero() {
		Assert.assertEquals(0, fibonachi.calculate(0));
	}

	@Test
	public void whenNEquals1ThenReturn1(){
		Assert.assertEquals(1, fibonachi.calculate(1));
	}

	@Test
	public void whenNEquals2ThenReturn1(){
		Assert.assertEquals(1, fibonachi.calculate(2));
	}

	@Test
	public void whenNEquals3ThenReturn2(){
		Assert.assertEquals(2, fibonachi.calculate(3));
	}

	@Test
	public void whenCalculateFibonachiThenResultIsPlusOfNMinus1AndNMinus2(){
		Assert.assertEquals(fibonachi.calculate(10), fibonachi.calculate(9) + fibonachi.calculate(8));
	}
}