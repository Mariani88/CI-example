package com.mycompany.app;

public class Fibonachi {

	public int calculate(int n) {

		if (n >= 2) {
			return calculateForMoreTwo(n);
		}

		return calculateForInferiorLimits(n);
	}

	private int calculateForMoreTwo(int n) {
		int resultNMinus2 = 0;
		int resultNMinus1 = 1;
		int result = 0;

		for (int index = 2; index <= n; index++) {
			result = resultNMinus1 + resultNMinus2;
			resultNMinus2 = resultNMinus1;
			resultNMinus1 = result;
		}

		return result;
	}

	private int calculateForInferiorLimits(int n) {
		return n;
	}

}